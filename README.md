# SkySkrape

A simple script to navigate the Sherdog MMA database. Currently only grabs
links off the event page, but hopefully deeper navigation to come.

## License

Copyright © 2012 Dimas Guardado, Jr.

Distributed under the Eclipse Public License, the same as Clojure.
