(ns skyskrape.core
  (:require [net.cgrand.enlive-html :as html])
  (:gen-class))

(def ^:dynamic *base-url* "http://www.sherdog.com")

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(defn select-event-links []
  (html/select (fetch-url (str *base-url* "/events/"))
               [(html/attr= :itemprop "url")]))

(defn fetch-event-info [url]
  (html/select (fetch-url url)
               [:div.event_detail :h1 (html/attr= :itemprop "name")]))

(defn print-event-list []
  (doseq [link (map (comp #(str *base-url* %) :href :attrs) 
                    (select-event-links))]
    (println (first (:content (first (fetch-event-info link)))))))

(defn -main
  "Scrapes Sherdog.com"
  [& args]
  (print-event-list))
