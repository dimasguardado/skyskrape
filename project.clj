(defproject humandriven/skyskrape "1-SNAPSHOT"
  :description "Scrapes Sherdog.com for fighter information"
  :url "http://github.com/dguardado/skyskrape"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [enlive "1.0.1"]]
  :main skyskrape.core)
